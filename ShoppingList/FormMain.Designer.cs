﻿namespace ShoppingList
{
    partial class FormMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            this.listBoxShoppingList = new System.Windows.Forms.ListBox();
            this.buttonAddProduct = new System.Windows.Forms.Button();
            this.buttonRemoveProduct = new System.Windows.Forms.Button();
            this.labelTotalCost = new System.Windows.Forms.Label();
            this.buttonSerializable = new System.Windows.Forms.Button();
            this.buttonDeserialize = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label1.Location = new System.Drawing.Point(192, 16);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(197, 29);
            label1.TabIndex = 0;
            label1.Text = "Список покупок";
            // 
            // listBoxShoppingList
            // 
            this.listBoxShoppingList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBoxShoppingList.FormattingEnabled = true;
            this.listBoxShoppingList.ItemHeight = 16;
            this.listBoxShoppingList.Location = new System.Drawing.Point(16, 88);
            this.listBoxShoppingList.Name = "listBoxShoppingList";
            this.listBoxShoppingList.Size = new System.Drawing.Size(568, 292);
            this.listBoxShoppingList.TabIndex = 1;
            // 
            // buttonAddProduct
            // 
            this.buttonAddProduct.Location = new System.Drawing.Point(40, 392);
            this.buttonAddProduct.Name = "buttonAddProduct";
            this.buttonAddProduct.Size = new System.Drawing.Size(180, 30);
            this.buttonAddProduct.TabIndex = 2;
            this.buttonAddProduct.Text = "Добавить к покупкам";
            this.buttonAddProduct.UseVisualStyleBackColor = true;
            this.buttonAddProduct.Click += new System.EventHandler(this.buttonAddProduct_Click);
            // 
            // buttonRemoveProduct
            // 
            this.buttonRemoveProduct.Location = new System.Drawing.Point(368, 392);
            this.buttonRemoveProduct.Name = "buttonRemoveProduct";
            this.buttonRemoveProduct.Size = new System.Drawing.Size(180, 30);
            this.buttonRemoveProduct.TabIndex = 3;
            this.buttonRemoveProduct.Text = "Удалить из покупок";
            this.buttonRemoveProduct.UseVisualStyleBackColor = true;
            this.buttonRemoveProduct.Click += new System.EventHandler(this.buttonRemoveProduct_Click);
            // 
            // labelTotalCost
            // 
            this.labelTotalCost.AutoSize = true;
            this.labelTotalCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTotalCost.Location = new System.Drawing.Point(376, 64);
            this.labelTotalCost.Name = "labelTotalCost";
            this.labelTotalCost.Size = new System.Drawing.Size(154, 16);
            this.labelTotalCost.TabIndex = 4;
            this.labelTotalCost.Text = "Общая сумма покупок:";
            // 
            // buttonSerializable
            // 
            this.buttonSerializable.Location = new System.Drawing.Point(40, 432);
            this.buttonSerializable.Name = "buttonSerializable";
            this.buttonSerializable.Size = new System.Drawing.Size(180, 30);
            this.buttonSerializable.TabIndex = 5;
            this.buttonSerializable.Text = "Сохранить список";
            this.buttonSerializable.UseVisualStyleBackColor = true;
            this.buttonSerializable.Click += new System.EventHandler(this.buttonSerializable_Click);
            // 
            // buttonDeserialize
            // 
            this.buttonDeserialize.Location = new System.Drawing.Point(368, 432);
            this.buttonDeserialize.Name = "buttonDeserialize";
            this.buttonDeserialize.Size = new System.Drawing.Size(180, 30);
            this.buttonDeserialize.TabIndex = 6;
            this.buttonDeserialize.Text = "Загрузить список";
            this.buttonDeserialize.UseVisualStyleBackColor = true;
            this.buttonDeserialize.Click += new System.EventHandler(this.buttonDeserialize_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 480);
            this.Controls.Add(this.buttonDeserialize);
            this.Controls.Add(this.buttonSerializable);
            this.Controls.Add(this.labelTotalCost);
            this.Controls.Add(this.buttonRemoveProduct);
            this.Controls.Add(this.buttonAddProduct);
            this.Controls.Add(this.listBoxShoppingList);
            this.Controls.Add(label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Список покупок";
            this.Activated += new System.EventHandler(this.FormMain_Activated);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxShoppingList;
        private System.Windows.Forms.Button buttonAddProduct;
        private System.Windows.Forms.Button buttonRemoveProduct;
        private System.Windows.Forms.Label labelTotalCost;
        private System.Windows.Forms.Button buttonSerializable;
        private System.Windows.Forms.Button buttonDeserialize;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}

