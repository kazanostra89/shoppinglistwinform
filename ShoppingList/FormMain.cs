﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShoppingList
{
    public partial class FormMain : Form
    {
        private FormProduct formProduct;
        private ShoppingList shoppingList;

        public FormMain()
        {
            InitializeComponent();

            MyInitializedComponents();
        }

        private void MyInitializedComponents()
        {
            formProduct = new FormProduct();

            shoppingList = new ShoppingList();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            listBoxShoppingList.DataSource = shoppingList.BindingList;

            CheckZeroListBox();
        }

        private void FormMain_Activated(object sender, EventArgs e)
        {
            if (formProduct.Product != null)
            {
                shoppingList.AddProduct(formProduct.Product);

                formProduct.ResetValueProduct();

                listBoxShoppingList.ClearSelected();

                CheckZeroListBox();
            }
        }

        private void buttonAddProduct_Click(object sender, EventArgs e)
        {
            formProduct.ShowDialog();
        }

        private void buttonRemoveProduct_Click(object sender, EventArgs e)
        {
            if (listBoxShoppingList.SelectedIndex != -1)
            {
                shoppingList.RemoveIndexProduct(listBoxShoppingList.SelectedIndex);

                CheckZeroListBox();
            }
            else
            {
                MessageBox.Show("Нет выбранных элементов!", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void CheckZeroListBox()
        {
            if (shoppingList.Count == 0)
            {
                buttonRemoveProduct.Enabled = false;
                buttonSerializable.Enabled = false;
            }
            else
            {
                buttonRemoveProduct.Enabled = true;
                buttonSerializable.Enabled = true;
            }

            labelTotalCost.Text = "Общая сумма покупок, руб: " + shoppingList.GetAllTotalCost();
        }

        private void buttonSerializable_Click(object sender, EventArgs e)
        {
            try
            {
                shoppingList.ListSerializable();
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Предупреждение!", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }

            MessageBox.Show("Сохранено успешно!");
        }

        private void buttonDeserialize_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                try
                {
                    listBoxShoppingList.DataSource = shoppingList.ListDeserialize(openFileDialog.FileName);
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message, "Предупреждение!", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    return;
                }

                CheckZeroListBox();

                MessageBox.Show("Загружено успешно!");
            }
        }

    }
}
