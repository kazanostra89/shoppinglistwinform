﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShoppingList
{
    public partial class FormProduct : Form
    {
        private Product product;
        private AutoCompleteStringCollection stringCollection;

        public FormProduct()
        {
            InitializeComponent();

            MyInitializedComponents();
        }

        public void MyInitializedComponents()
        {
            product = null;

            stringCollection = new AutoCompleteStringCollection();

            textBoxNameProduct.AutoCompleteCustomSource = stringCollection;
        }

        private void FormProduct_Load(object sender, EventArgs e)
        {
            labelStatus.Text = String.Empty;
        }

        private void textBoxNameProduct_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= (char)1040 && e.KeyChar <= (char)1103) || e.KeyChar == (char)Keys.Back)
            {
                return;
            }

            e.Handled = true;
        }

        private void textBoxPriceProduct_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= (char)48 && e.KeyChar <= (char)57) || e.KeyChar == (char)Keys.Back)
            {
                return;
            }

            if (e.KeyChar == '.')
            {
                e.KeyChar = ',';

                if (textBoxPriceProduct.Text.Contains(",") || textBoxPriceProduct.TextLength == 0)
                {
                    e.Handled = true;
                }

                return;
            }

            e.Handled = true;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (textBoxPriceProduct.TextLength != 0 && textBoxNameProduct.TextLength != 0 && UpDownQuantityProduct.Value != 0)
            {
                string name = textBoxNameProduct.Text;
                int quantity = 0;
                double price = 0;

                try
                {
                    quantity = Convert.ToInt32(UpDownQuantityProduct.Value);

                    price = Convert.ToDouble(textBoxPriceProduct.Text);
                }
                catch (Exception error)
                {
                    labelStatus.Text = error.Message;

                    return;   
                }

                product = new Product(name, quantity, price);

                if (!stringCollection.Contains(name))
                {
                    stringCollection.Add(name);
                }

                this.Close();
            }
            else
            {
                labelStatus.Text = "Операция невозможна! Не все поля заполнены";
            }
        }

        public void ResetValueProduct()
        {
            product = null;
        }
        

        public Product Product
        {
            get { return product; }
        }

        private void FormProduct_FormClosing(object sender, FormClosingEventArgs e)
        {
            textBoxNameProduct.Text = String.Empty;
            textBoxPriceProduct.Text = String.Empty;
            UpDownQuantityProduct.Value = Decimal.Zero;
        }
    }
}
