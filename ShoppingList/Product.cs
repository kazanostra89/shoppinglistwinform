﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingList
{
    [Serializable]
    public class Product : IEquatable<Product>
    {
        private string name;
        private int quantity;
        private double price;
        private double totalCost;
        
        public Product(string name, int quantity, double price)
        {
            this.name = name;
            this.quantity = quantity;
            this.price = price;

            totalCost = quantity * price;
        }

        public void ResetTotalCost(int quantity)
        {
            this.quantity += quantity;

            totalCost = this.quantity * price;
        }

        public override string ToString()
        {
            return name + " - " + quantity + " штк.; Цена за ед. - " + price + " руб.; Общая стоим. = " + totalCost + " руб.";
        }

        public bool Equals(Product other)
        {
            return this.name.Equals(other.name) && this.price.Equals(other.price);
        }


        public string Name
        {
            get { return name; }
        }

        public int Quantity
        {
            get { return quantity; }

            set { quantity = value; }
        }

        public double Price
        {
            get { return price; }
        }

        public double TotalCost
        {
            get { return totalCost; }
        }

    }
}
