﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;


namespace ShoppingList
{
    class ShoppingList
    {
        private BindingList<Product> shoppingList;
        private BinaryFormatter binFormater;

        public ShoppingList()
        {
            shoppingList = new BindingList<Product>();

            binFormater = new BinaryFormatter();
        }

        public void AddProduct(Product product)
        {
            if (shoppingList.Contains(product))
            {
                shoppingList.First(item => item.Equals(product)).ResetTotalCost(product.Quantity);

                shoppingList.ResetBindings();
            }
            else
            {
                shoppingList.Add(product);
            }
        }

        public void RemoveIndexProduct(int index)
        {
            shoppingList.RemoveAt(index);
        }

        public double GetAllTotalCost()
        {
            return shoppingList.Sum(item => item.TotalCost);
        }

        public void ListSerializable()
        {
            using (FileStream strem = new FileStream("ShoppingList.dat", FileMode.OpenOrCreate))
            {
                binFormater.Serialize(strem, shoppingList);
            }
        }

        public object ListDeserialize(string path)
        {
            using (FileStream strem = new FileStream(path, FileMode.OpenOrCreate))
            {
                shoppingList = binFormater.Deserialize(strem) as BindingList<Product>;
            }

            return shoppingList;
        }


        public object BindingList
        {
            get { return shoppingList; }
        }

        public int Count
        {
            get { return shoppingList.Count; }
        }

    }
}
